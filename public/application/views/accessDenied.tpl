{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $LoginPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=text]').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    $('#contact').focus();
   
    /* =======================================================
     *
     * set tab on return for input elements with foprm submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
    
});

function validateForm() {
    if ($('#contact').val()=='') {
        $('#error').html("{$page['Errors']['input_error']}").show('slow');
        $('#contact').focus();
        return false;
    }
    return true;
}
</script>

{/block}

{block name=body}
<div class="main" id="accessdenied">
    
        <form id="accessDeniedForm" name="accessDeniedForm" method="post" action="{$_subdomain}/Login/accessDenied" class="prepend-5 span-14 last">
            
            <fieldset>

                <legend title="IM5005">{$page['Text']['legend']|escape:'html'}</legend>
                {* <div class="serviceInstructionsBar">IM5005</div> *}
                
                <p class="information">
                    {$page['Text']['max_login']|escape:'html'} {$maxLoginAttempts} {$page['Text']['attempts']|escape:'html'}
                </p>
                
                <p class="information">
                   {$page['Text']['report_issue']|escape:'html'}
                </p>
                
                <p style="text-align: center;">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <a href="javascript:if (validateForm()) document.accessDeniedForm.submit();" tabIndex="2">{$page['Buttons']['submit']|escape:'html'}</a>
                    </span>
                </p>
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="3">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
        
                <p class="information" style="text-align: center; margin-bottom: 10px;">
                    {$page['Text']['login_issue']|escape:'html'}
                </p>
		
                <p style="text-align: center;">
                    <label for="contact">{$page['Labels']['contact']|escape:'html'}</label>
                    <input type="text" name="contact" id="contact" value="" class="text auto-submit" tabIndex="1" />
                </p>
                
                {if $error eq ''}
                <p id="error" class="formError" style="display: none; text-align: center;" />
                {else}
                <p id="error" class="formError" style="text-align: center;" >{$error|escape:'html'}</p>
                {/if}

                              
            </fieldset>
  
        </form>
    
</div>
{/block}